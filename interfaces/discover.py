import graphene
from providers import musicbrainz, lastfm, spotify, youtube


##########
# MODELS #
##########

class Track(graphene.ObjectType):
    name = graphene.String()
    duration = graphene.String()
    artists = graphene.String()
    youtube_id = graphene.String()

    def resolve_youtube_id(self, args, context, info):
        return youtube.get_track_id(self.artists, self.name, self.duration)


##############
# ROOT QUERY #
##############

class Query(graphene.ObjectType):
    search_music = graphene.List(Track,
                                 artist=graphene.String(default_value=''),
                                 track=graphene.String(default_value=''))
    search_tracks_musicbrainz = \
        graphene.List(Track,
                      artist=graphene.String(default_value=''),
                      track=graphene.String(default_value=''))
    search_tracks_spotify = \
        graphene.List(Track,
                      artist=graphene.String(default_value=''),
                      track=graphene.String(default_value=''))
    search_tracks_lastfm = \
        graphene.List(Track,
                      artist=graphene.String(default_value=''),
                      track=graphene.String(default_value=''))

    def resolve_search_music(self, args, context, info):
        return musicbrainz.search_tracks(args['track'], args['artist'])

    def resolve_search_tracks_musicbrainz(self, args, context, info):
        return musicbrainz.search_tracks(args['track'], args['artist'])

    def resolve_search_tracks_spotify(self, args, context, info):
        return spotify.search_tracks('{artist} {track}'.format(**args))

    def resolve_search_tracks_lastfm(self, args, context, info):
        if args['track'] is '':
            return lastfm.search_tracks_by_artist(args['artist'])
        else:
            return lastfm.search_tracks(args['track'], args['artist'])


##########
# SCHEMA #
##########

schema = graphene.Schema(
    query=Query,)
