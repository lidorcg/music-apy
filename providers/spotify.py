"""This is the Spotify API module.

This module search spotify for songs and artists
and retrieve their metadata.
"""

import spotipy

from interfaces import discover

sp = spotipy.Spotify()


def search_tracks(q):
    tracks_json = sp.search(q=q, limit=10, type='track')['tracks']['items']
    return list(map(process_track, tracks_json))


#####################
# utility functions #
#####################

def process_track(track):
    artists = ", ".join(map(lambda a: a['name'], track['artists']))
    return discover.Track(name=track['name'],
                          duration=track['duration_ms'],
                          artists=artists)
