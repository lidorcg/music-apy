"""This is the musicbrainz API module.

This module search musicbrainz for songs and artists
and retrieve their metadata.
"""

import musicbrainzngs as mb

from interfaces import discover

mb.set_useragent("my-music", "0.1", "localhost")


def search_tracks(track, artist):
    tracks = mb.search_recordings(recording=track, artist=artist, limit=10)[
        'recording-list']
    filtered_tracks = filter(lambda t: 'length' in t, tracks)
    return list(map(process_track, filtered_tracks))


def process_track(trk):
    return discover.Track(name=trk['title'],
                          duration=trk['length'],
                          artists=process_artists(trk['artist-credit']))


def process_artists(artists):
    filtered_artists = filter(lambda a: type(a) is dict, artists)
    return ', '.join(map(lambda a: a['artist']['name'], filtered_artists))
